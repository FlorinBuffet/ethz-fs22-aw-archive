import algorithms.*;
import java.util.*;

class Main {
    public static void main(String[] args) {
        // Uncomment this line if you want to read from a file
        // In.open("public/test1.in");
        // Out.compareTo("public/test1.out");

        int t = In.readInt();
        for (int i = 0; i < t; i++) {
            testCase();
        }
        
        // Uncomment this line if you want to read from a file
        // In.close();
    }

    public static void testCase() {
        int n = In.readInt();
        int m = In.readInt();
        int v = In.readInt();
        
        int[][] edges = new int[n][m];
        int[] distance = new int[n];
        
        for (int i = 0; i<n; i++){
          for (int j = 0; j<m; j++){
            edges[i][j] = -1;
          }
        }
        
        
        for (int i = 0; i<m; i++){
          int edgev = In.readInt();
          int edgeu = In.readInt();
          edges[edgev][distance[edgev]] = edgeu;
          edges[edgeu][distance[edgeu]] = edgev;
          distance[edgev]++;
          distance[edgeu]++;
        }
        
        for (int i = 0; i<n; i++){
          distance[i] = -1;
        }
        distance[v] = 0;
        
        List<Integer> queue = new ArrayList<Integer>();
        queue.add(v);
        
        while (queue.size() > 0 ){
          int workingVertice = queue.get(0);
          queue.remove(0);
          for (int i = 0; i<edges[workingVertice].length; i++){
            int temp = edges[workingVertice][i];
            if (temp!=-1){
              if (distance[temp]==-1){
                distance[temp] = distance[workingVertice]+1;
                queue.add(temp);
              }
            }
          }
        }


        String output = "";
        int counter = 0;
        for (int i = 0; i<distance.length; i++){
          output = output+distance[i];
          if(i<distance.length-1){
            output = output + " ";
          }
        }

        
        // Output using Out.java class
        Out.println(output);
    }
}
